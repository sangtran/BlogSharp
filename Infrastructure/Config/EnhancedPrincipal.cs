﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Security.Principal;
using BlogSharp.Models;

namespace BlogSharp.Infrastructure.Config
{
    public class EnhancedPrincipal : IPrincipal
    {
        private readonly IIdentity identity;
        private readonly User userData;
        public static readonly EnhancedPrincipal Anonymous = new AnonymousPrincipal();

        public EnhancedPrincipal(IIdentity identity, User userData)
        {
            this.identity = identity;
            this.userData = userData;
        }

        public bool IsInRole(string role)
        {
            return role == "owner" && userData.IsOwner;
        }

        public IIdentity Identity
        {
            get { return identity; }
        }

        public User Data
        {
            get { return userData; }
        }

        private class AnonymousIdentity : IIdentity
        {
            public string Name
            {
                get { return "Anonymous"; }
            }

            public string AuthenticationType
            {
                get { return null; }
            }

            public bool IsAuthenticated
            {
                get { return false; }
            }
        }

        private class AnonymousPrincipal : EnhancedPrincipal
        {
            public AnonymousPrincipal()
                : base(new AnonymousIdentity(), new User
                {
                    ID = 0,
                    DisplayName = "Anonymous"
                })
            {
            }
        }
    }
}