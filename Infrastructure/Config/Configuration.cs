﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Configuration;

namespace BlogSharp.Infrastructure.Config
{
    public class Configuration : ConfigurationSection
    {
        public static Configuration Instance = (Configuration)ConfigurationManager.GetSection("blogsharp");

        [ConfigurationProperty("postPerPage", IsRequired = false, DefaultValue = 10)]
        public int PostPerPage
        {
            get { return (int)this["postPerPage"]; }
            set { this["postPerPage"] = value; }
        }
    }
}