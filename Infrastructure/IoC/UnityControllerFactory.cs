﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlogSharp.Infrastructure.IoC
{
    public class UnityControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(RequestContext requestContext,
                                                     string controllerName)
        {
            var provider = HttpContext.Current.ApplicationInstance as IContainerProvider;
            if (provider == null)
                throw new InvalidOperationException("Application class must implement IContainerProvider");
            return provider.Container.Resolve<IController>(controllerName);
        }
    }
}