﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System;
using System.Web;
using Microsoft.Practices.Unity;

namespace BlogSharp.Infrastructure.IoC
{
    public class PerRequestLifetimeManager : LifetimeManager
    {
        private readonly Guid key;

        public PerRequestLifetimeManager()
        {
            key = Guid.NewGuid();
        }

        public override object GetValue()
        {
            return HttpContext.Current.Items[key];
        }

        public override void SetValue(object newValue)
        {
            HttpContext.Current.Items[key] = newValue;
        }

        public override void RemoveValue()
        {
            HttpContext.Current.Items.Remove(key);
        }
    }
}