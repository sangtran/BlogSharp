﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using Microsoft.Practices.Unity;

namespace BlogSharp.Infrastructure.IoC
{
    internal interface IContainerProvider
    {
        IUnityContainer Container { get; }
    }
}