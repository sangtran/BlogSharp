﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Web.Mvc;
using BlogSharp.Controllers;

namespace BlogSharp.Infrastructure.Extensions
{
    public class OwnerOnlyAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controller = filterContext.Controller as BaseController;
            if (controller == null)
                return;
            if (!controller.CurrentUser.IsOwner)
                filterContext.Result = controller.RedirectToLogin();
        }
    }
}
