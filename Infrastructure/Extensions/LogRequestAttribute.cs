﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Web.Mvc;
using log4net;

namespace BlogSharp.Infrastructure.Extensions
{
    public class LogRequestAttribute : ActionFilterAttribute, IExceptionFilter
    {
        private static readonly ILog Log = LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var actionName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + "#" +
                             filterContext.ActionDescriptor.ActionName;
            Log.Debug("Executing action: " + actionName);
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var actionName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + "#" +
                             filterContext.ActionDescriptor.ActionName;
            Log.Debug("Executed action: " + actionName);
            base.OnActionExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Log.Debug("Executing result: " + filterContext.Result.GetType());
            base.OnResultExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Log.Debug("Executed result: " + filterContext.Result.GetType());
            base.OnResultExecuted(filterContext);
        }

        public void OnException(ExceptionContext filterContext)
        {
            Log.Warn("Unknown error occurred", filterContext.Exception);
        }
    }
}