﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;

namespace BlogSharp.Infrastructure.Extensions
{
    public class RssResult : ActionResult
    {
        private readonly SyndicationFeed feed;

        public RssResult(SyndicationFeed feed)
        {
            this.feed = feed;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/rss+xml";
            var formatter = new Rss20FeedFormatter(feed);
            using (var writer = XmlWriter.Create(context.HttpContext.Response.Output))
            {
                formatter.WriteTo(writer);
            }
        }
    }
}