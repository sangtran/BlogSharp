﻿// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)

var functionQueue = (function() {
    var functionList = [];
    return {
        add: function(callback) {
            functionList.push(callback);
        },
        execute: function() {
            for (var i = 0; functionList[i]; i++) {
                functionList[i]();
            }
        }
    }
})();

function onLoad(callback) {
    functionQueue.add(callback);
}