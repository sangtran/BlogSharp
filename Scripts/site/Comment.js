﻿// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)

var comments = function($) {
    var setupFormValidation = function(formSelector, validationRules, validationMessages, onSubmitCallback) {
        enableSubmitButton(formSelector, true);
        $(formSelector).validate({
            rules: (validationRules || {}),
            messages: (validationMessages || {}),
            errorElement: "span",
            errorClass: "input-validation-error",
            errorPlacement: function(error, element) { },
            submitHandler: function(form) {
                enableSubmitButton(formSelector, false);
                if (onSubmitCallback)
                    onSubmitCallback();
                else
                    form.submit();
                enableSubmitButton(formSelector, true);
            }
        });
    };

    var enableSubmitButton = function(formSelector, isEnabled) {
        $(formSelector).find("input[type='submit']").attr("disabled", isEnabled ? "" : "true");
    };

    var getContainer = function() {
        return $("#commentlist");
    };

    var hideLoader = function() {
        $("img.ajax-loader").remove();
    }

    var showLoader = function() {
        getContainer().append('<img class="ajax-loader" src="/content/images/ajax-loader.gif" title="loading..." alt="loading..." />');
    };

    var canPostComments = function(container) {
        var hidden = container.find("#can-post-comments");
        return hidden.val().toLowerCase() === "true";
    };

    var renderForm = function(entryID, container) {
        var formId = "form-comments";
        if (canPostComments(container)) {
            if (container.find("#" + formId).length === 0) {
                var form = '';
                form += '<form id="' + formId + '" class="post-comments"><div>';
                form += '   <textarea id="comment" name="comment"></textarea>';
                form += '   <input class="post-comment-button" type="submit" value="Add Comment" />';
                form += '</form>';
                container.append(form);
                setupFormValidation("#" + formId,
                    { comment: { required: true} },
                    { comment: { required: "Content is required"} },
                    function() { postComment(entryID, formId); });
            }
        }
    };

    var getComments = function(entryID, container) {
        showLoader(entryID);
        $.ajax({
            type: "POST",
            url: "/entries/" + entryID + "/comments",
            dataType: "json",
            success: function(json) {
                showComments(entryID, json);
            }
        });
    };

    var showComments = function(entryID, json) {
        var container = getContainer();
        commentList = container.find("ol#thecomments");
        commentList.children().remove();
        var plural = json && json.length > 1;
        $("#comment-count").text("There " + (plural ? "are " : "is ") + json.length + (plural ? " comments" : " comment"));
        if (json && json.length > 0) {
            var html = "";
            for (var i = 0; json[i]; i++)
                html += getCommentHtml(json[i]);
            commentList.append(html);
            commentList.children().show();
        }
        hideLoader();
    };

    var getCommentHtml = function(json) {
        var actionHtml = json.CanBeDeleted
            ? '<img onclick=\'comments.deleteComment(' + json.EntryID + ', "' + json.DeleteUrl + '")\' src="/content/images/ico-delete.gif" />'
            : '';
        var html = '';
        html += '<li id="comment-' + json.ID + '" style="display:none" class="comment">';
        html += '   <div class="author">';
        html += '       <div class="pic"><img src=' + json.AvatarUrl + ' class="avatar avatar-32 photo" height="32" width="32" /></div>';
        html += '       <div class="name">' + json.UserName + '</div>';
        html += '   </div>';
        html += '   <div class="info">';
        html += '       <div class="date">' + json.CreatedDate + '</div>';
        html += '       <div class="act">' + actionHtml + '</div>';
        html += '       <div class="fixed"></div>';
        html += '       <div class="content">' + json.Content + '</div>';
        html += '   </div>';
        html += '</li>';
        return html;
    };

    var postComment = function(entryID, formId) {
        showLoader();
        var formSelector = "#" + formId;
        var textarea = $(formSelector + " textarea");
        $.ajax({
            type: "POST",
            url: "/entries/" + entryID + "/comment",
            dataType: "json",
            data: { content: textarea.val() },
            success: function(json) {
                textarea.val("");
                showComments(entryID, json);
            }
        });
    };

    return {
        init: function(entryID) {
            var container = getContainer();
            getComments(entryID, container);
            renderForm(entryID, container);
            container.show();
        },

        deleteComment: function(entryID, deleteUrl) {
            if (confirm("Are you sure you want to delete this comment?")) {
                showLoader();
                $.post(deleteUrl, { entryID: entryID }, function(json) {
                    showComments(entryID, json);
                }, "json");
            }
        }
    };
} (jQuery);
