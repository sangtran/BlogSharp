﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlogSharp.Infrastructure.Extensions;
using BlogSharp.Infrastructure.Utils;

namespace BlogSharp.ViewModels
{
    [PropertiesMustMatch("Password", "ConfirmPassword", ErrorMessage = "The password and confirmation password do not match")]
    public class RegisterModel
    {
        [DisplayName("Login Name")]
        [Required(ErrorMessage = "Login name is required")]
        [StringLength(64, ErrorMessage = "Login name cannot be longer than 64 characters")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(64, ErrorMessage = "Password cannot be longer than 64 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Confirm Password")]
        [Required(ErrorMessage = "Confirm password is required")]
        [StringLength(64, ErrorMessage = "Confirm password cannot be longer than 64 characters")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [DisplayName("Display Name")]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(64, ErrorMessage = "Email cannot be longer than 64 characters")]
        [RegularExpression(PresentationUtils.EmailPattern, ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [StringLength(64, ErrorMessage = "Website cannot be longer than 64 characters")]
        [RegularExpression(PresentationUtils.UrlPattern, ErrorMessage = "Invalid website URL")]
        public string Website { get; set; }
    }
}
