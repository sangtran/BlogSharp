﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BlogSharp.ViewModels
{
    public class BlogEntryModel
    {
        [HiddenInput(DisplayValue = false)]
        public int? ID { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(256, ErrorMessage = "Title cannot be longer than 256 characters")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Required(ErrorMessage = "At least 1 tag must be specified")]
        public string Tags { get; set; }

        [ScaffoldColumn(false)]
        public string UserName { get; set; }

        [ScaffoldColumn(false)]
        public DateTime PublishedDate { get; set; }
    }
}
