﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.ComponentModel.DataAnnotations;

namespace BlogSharp.ViewModels
{
    public class SetupModel : RegisterModel
    {
        [Required(ErrorMessage = "About is required")]
        [ScaffoldColumn(false)]
        [DataType(DataType.MultilineText)]
        public string About { get; set; }
    }
}
