﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BlogSharp.ViewModels
{
    public class LogOnModel
    {
        [DisplayName("Login Name")]
        [Required(ErrorMessage = "Login name is required")]
        [StringLength(64, ErrorMessage = "Login name cannot be longer than 64 characters")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(64, ErrorMessage = "Password cannot be longer than 64 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ReturnUrl { get; set; }

        [DisplayName("Remember Me")]
        [ScaffoldColumn(false)]
        public bool RememberMe { get; set; }
    }
}
