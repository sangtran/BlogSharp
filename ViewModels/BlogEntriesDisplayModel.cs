﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Collections.Generic;
using BlogSharp.Models;

namespace BlogSharp.ViewModels
{
    public class BlogEntriesDisplayModel
    {
        public PagerModel PagerModel { get; set; }
        public IEnumerable<BlogEntry> Entries { get; set; }
    }
}
