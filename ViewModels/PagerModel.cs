﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

namespace BlogSharp.ViewModels
{
    public class PagerModel
    {
        public const string PageSymbol = "_PAGE_";

        public int EntriesPerPage { get; set; }
        public int CurrentPageIndex { get; set; }
        public int TotalEntries { get; set; }
        public string PageUrlTemplate { get; set; }
    }
}
