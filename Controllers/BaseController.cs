﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Web.Mvc;
using BlogSharp.Infrastructure.Config;
using BlogSharp.Infrastructure.Extensions;
using BlogSharp.Models;

namespace BlogSharp.Controllers
{
    [HandleError]
    [LogRequest]
    public class BaseController : Controller
    {
        public User CurrentUser
        {
            get
            {
                var principal = User as EnhancedPrincipal;
                return principal == null ? null : principal.Data;
            }
        }

        public ActionResult RedirectToLogin()
        {
            return RedirectToAction("LogOn", "Account", new { returnUrl = Request.Path });
        }
    }
}
