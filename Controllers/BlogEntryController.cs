﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using BlogSharp.Infrastructure.Config;
using BlogSharp.Infrastructure.Extensions;
using BlogSharp.Infrastructure.Utils;
using BlogSharp.Models;
using BlogSharp.ViewModels;
using Microsoft.Practices.Unity;

namespace BlogSharp.Controllers
{
    public class BlogEntryController : BaseController
    {
        [Dependency]
        public IBlogSharpRepository Repository { get; set; }

        [OwnerOnly]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Show(int id)
        {
            var entry = Repository.FindEntryByID(id);
            var model = new BlogEntryModel
            {
                ID = id,
                Title = entry.Title,
                Content = entry.Content,
                Tags = PresentationUtils.GetTagString(entry.Tags),
                UserName = entry.User.DisplayName,
                PublishedDate = entry.PublishedDate
            };
            return View(model);
        }

        [OwnerOnly]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(BlogEntryModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            var entry = new BlogEntry
            {
                Title = model.Title,
                Content = model.Content,
                ModifiedDate = DateTime.Now,
                PublishedDate = DateTime.Now,
                User = Repository.GetBlogOwner()
            };
            Repository.AddBlogEntry(entry);
            AddTagsToEntry(model.Tags, entry);
            Repository.CommitChanges();
            return RedirectToAction("Show", new { entry.ID });
        }

        [OwnerOnly]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var entry = Repository.FindEntryByID(id);
            var model = new BlogEntryModel
            {
                ID = id,
                Title = entry.Title,
                Content = entry.Content,
                Tags = PresentationUtils.GetTagString(entry.Tags),
                UserName = entry.User.DisplayName,
                PublishedDate = entry.PublishedDate
            };
            return View(model);
        }

        [OwnerOnly]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(BlogEntryModel model, int id)
        {
            if (!ModelState.IsValid)
                return View(model);
            var entry = Repository.FindEntryByID(model.ID.Value);
            entry.ModifiedDate = DateTime.Now;
            entry.Title = model.Title;
            entry.Content = model.Content;
            AddTagsToEntry(model.Tags, entry);
            Repository.CommitChanges();
            return RedirectToAction("Show", new { entry.ID });
        }

        private void AddTagsToEntry(string tagsString, BlogEntry entry)
        {
            entry.Tags.Clear();
            var tags = string.IsNullOrEmpty(tagsString)
                           ? null
                           : Repository.FindTagsByNames(PresentationUtils.ParseTagsString(tagsString));
            if (tags != null)
                tags.ToList().ForEach(tag => entry.Tags.Add(tag));
        }

        [OwnerOnly]
        [HttpPost]
        public ActionResult Delete(int entryID)
        {
            Repository.DeleteBlogEntry(entryID);
            Repository.CommitChanges();
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Index([DefaultValue(0)] int pageIndex)
        {
            var owner = Repository.GetBlogOwner();
            if (owner == null)
            {
                return RedirectToAction("Setup", "Account");
            } 
            int entriesPerPage = Configuration.Instance.PostPerPage;
            return ShowEntries(
                    () => Repository.FindEntriesInPage(pageIndex, entriesPerPage),
                    () => Repository.GetEntriesCount(),
                    pageIndex,
                    "Index",
                    entriesPerPage
                );
        }

        [HttpGet]
        public ActionResult ShowByTag(string tag, [DefaultValue(0)] int pageIndex)
        {
            int entriesPerPage = Configuration.Instance.PostPerPage;
            return ShowEntries(
                    () => Repository.FindEntriesByTag(tag, pageIndex, entriesPerPage),
                    () => Repository.GetEntriesInTagCount(tag),
                    pageIndex,
                    "ShowByTag",
                    entriesPerPage
                );
        }

        private ActionResult ShowEntries(Func<IEnumerable<BlogEntry>> filterFunc, Func<int> countFunc,
            int pageIndex, string actionName, int entriesPerPage)
        {
            var blogEntries = filterFunc();
            var blogEntriesCount = countFunc();
            var model = new BlogEntriesDisplayModel
            {
                Entries = blogEntries,
                PagerModel = new PagerModel
                {
                    CurrentPageIndex = pageIndex,
                    EntriesPerPage = entriesPerPage,
                    TotalEntries = blogEntriesCount,
                    PageUrlTemplate =
                        Url.Action(actionName, new { pageIndex = PagerModel.PageSymbol })
                }
            };
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult GetComments(int entryID)
        {
            // Explicit cast to IEnumerable<Comment> to allow the subsequent queries to be done in-memory
            IEnumerable<Comment> comments = Repository.FindEntryComments(entryID);
            var jsonComments = (from c in comments
                                select new
                                {
                                    c.ID,
                                    c.Content,
                                    EntryID = entryID,
                                    CreatedDate = c.CreatedDate.ToLongDateString(),
                                    UserName = c.User.DisplayName,
                                    AvatarUrl = PresentationUtils.GetGravatar(c.User.Email, new { width = "32", height = "32", d = "identicon", r = "pg" }),
                                    CanBeDeleted = CurrentUser.IsOwner,
                                    DeleteUrl = Url.Action("DeleteComment", "BlogEntry", new { commentId = c.ID, entryID })
                                }).ToList();
            return Json(jsonComments);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddComment(int entryID, string content)
        {
            var comment = new Comment
            {
                Content = content,
                User = CurrentUser,
                BlogEntry = Repository.FindEntryByID(entryID),
                CreatedDate = DateTime.Now
            };
            Repository.AddComment(comment);
            Repository.CommitChanges();
            return GetComments(entryID);
        }

        [HttpPost]
        public ActionResult DeleteComment(int entryID, int commentID)
        {
            if (CurrentUser.IsOwner)
            {
                Repository.DeleteComment(commentID);
                Repository.CommitChanges();
            }
            return GetComments(entryID);
        }

        public ActionResult ShowTagCloud()
        {
            var tags = Repository.FindAllTags().Where(t => t.TimesApplied > 0);
            return PartialView("TagCloud", tags);
        }

        [HttpGet]
        public ActionResult Rss()
        {
            var baseUrl = Url.GetBaseUrl();

            // Explicit cast to IEnumerable<Comment> to allow the subsequent queries to be done in-memory
            IEnumerable<BlogEntry> entries = Repository.FindAllEntries();
            var syndicationItems = entries.Select(entry => GetRssItem(baseUrl, entry));
            var feed = new SyndicationFeed("Blog#", "Entries from Blog#", Request.Url, syndicationItems.ToList());
            return new RssResult(feed);
        }

        private SyndicationItem GetRssItem(string baseUrl, BlogEntry entry)
        {
            var url = baseUrl + Url.Action("Show", "BlogEntry", new { id = entry.ID });
            return new SyndicationItem(entry.Title, entry.Content,
                new Uri(url), entry.ID.ToString(), new DateTimeOffset(entry.PublishedDate));
        }
    }
}
