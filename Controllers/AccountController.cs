﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Web.Mvc;
using System.Web.Security;
using BlogSharp.Infrastructure.Utils;
using BlogSharp.Models;
using BlogSharp.ViewModels;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using Microsoft.Practices.Unity;

namespace BlogSharp.Controllers
{
    public class AccountController : BaseController
    {
        [Dependency]
        public IBlogSharpRepository Repository { get; set; }

        [HttpGet]
        public ActionResult LogOn()
        {
            var model = new LogOnModel
            {
                ReturnUrl = Request.QueryString["ReturnUrl"]
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = Repository.ValidateUser(model.LoginName, model.Password.Encrypt());
            if (user != null)
            {
                return StoreAuthCookieAndRedirect(user.ID, model.ReturnUrl, model.RememberMe);
            }
            ModelState.AddModelError(string.Empty, "Invalid login name and/or password");
            return View("LogOn");
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult AuthenticateOpenID(LogOnModel model)
        {
            var openid = new OpenIdRelyingParty();
            var response = openid.GetResponse();
            if (response == null)
            {
                // Stage 2: user submits identifier (HTTP POST)
                Identifier id;
                if (Identifier.TryParse(Request.Form["openid_identifier"], out id))
                {
                    try
                    {
                        var fetch = new ClaimsRequest
                        {
                            Nickname = DemandLevel.Request,
                            Email = DemandLevel.Request,
                        };
                        var realm = new Realm(Url.GetBaseUrl());
                        var request = openid.CreateRequest(Request.Form["openid_identifier"], realm);
                        request.AddCallbackArguments("returnurl", model.ReturnUrl);
                        request.AddCallbackArguments("rememberme", model.RememberMe.ToString());
                        request.AddExtension(fetch);
                        return request.RedirectingResponse.AsActionResult();
                    }
                    catch (ProtocolException)
                    {
                        ModelState.AddModelError(string.Empty, "Unknown login error");
                        return View("LogOn");
                    }
                }

                ModelState.AddModelError(string.Empty, "Invalid identifier");
                return View("LogOn");
            }

            // Stage 3: OpenID Provider sends back response (HTTP GET)
            switch (response.Status)
            {
                case AuthenticationStatus.Authenticated:
                    var alias = response.FriendlyIdentifierForDisplay;
                    var fetch = response.GetExtension<ClaimsResponse>();
                    var email = fetch == null ? null : fetch.Email;
                    var claimedIdentifier = response.ClaimedIdentifier.ToString();

                    var user = Repository.FindUserByOpenID(claimedIdentifier);
                    if (user == null)
                    {
                        user = new User
                        {
                            OpenID = claimedIdentifier,
                            DisplayName = alias ?? claimedIdentifier,
                            Email = email,
                            IsOwner = false
                        };
                        Repository.AddUser(user);
                        Repository.CommitChanges();
                    }

                    var rememberArg = response.GetCallbackArgument("rememberme");
                    var rememberMe = string.IsNullOrEmpty(rememberArg)
                                   ? false
                                   : bool.Parse(rememberArg);
                    var returnUrl = response.GetCallbackArgument("returnurl");
                    return StoreAuthCookieAndRedirect(user.ID, returnUrl, rememberMe);
                case AuthenticationStatus.Canceled:
                    ModelState.AddModelError(string.Empty, "Cancelled by provider");
                    return View("LogOn");
                case AuthenticationStatus.Failed:
                    ModelState.AddModelError(string.Empty, "Invalid identifier");
                    return View("LogOn");
            }
            return new EmptyResult();
        }

        private ActionResult StoreAuthCookieAndRedirect(int userID, string returnUrl, bool remember)
        {
            FormsAuthentication.SetAuthCookie(userID.ToString(), remember);
            return string.IsNullOrEmpty(returnUrl)
                ? (ActionResult)RedirectToAction("Index", "BlogEntry")
                : Redirect(returnUrl.UrlDecode());
        }

        [HttpGet]
        public ActionResult LogOff(string returnUrl)
        {
            FormsAuthentication.SignOut();
            returnUrl = string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl;
            return Redirect(returnUrl.UrlDecode());
        }

        [HttpGet]
        public ActionResult Setup()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = Repository.FindUserByLoginName(model.LoginName);
            if (user != null)
            {
                ModelState.AddModelError("Login", "Login name already exist");
                return View(model);
            }
            var newUser = new User
            {
                LoginName = model.LoginName,
                Password = model.Password.Encrypt(),
                Website = model.Website,
                Email = model.Email,
                DisplayName = model.DisplayName ?? model.LoginName,
                IsOwner = false
            };
            Repository.AddUser(newUser);
            Repository.CommitChanges();
            return RedirectToLogin();
        }

        [HttpGet]
        public ActionResult About()
        {
            var owner = Repository.GetBlogOwner();
            if (owner == null)
            {
                return RedirectToAction("Setup", "Account");
            }
            return View(owner);
        }

        [HttpPost]
        public ActionResult Setup(SetupModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = Repository.FindUserByLoginName(model.LoginName);
            if (user != null)
            {
                ModelState.AddModelError("LoginName", "Login name already exist");
                return View(model);
            }
            var newUser = new User
            {
                LoginName = model.LoginName,
                Password = model.Password.Encrypt(),
                Website = model.Website,
                Email = model.Email,
                DisplayName = model.DisplayName ?? model.LoginName,
                IsOwner = true,
                About = model.About
            };
            Repository.AddUser(newUser);
            Repository.CommitChanges();
            return RedirectToLogin();
        }
    }
}
