﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System.Collections.Generic;
using System.Linq;

namespace BlogSharp.Models
{
    public interface IBlogSharpRepository
    {
        IQueryable<Tag> FindAllTags();
        IQueryable<Tag> FindTagsByNames(IEnumerable<string> tagNames);
        void AddTag(Tag tag);

        User GetBlogOwner();
        User ValidateUser(string loginName, string password);
        void AddUser(User user);
        User FindUserByLoginName(string loginName);
        User FindUserByOpenID(string openID);
        User FindUserByID(int userID);

        void AddBlogEntry(BlogEntry entry);
        void UpdateBlogEntry(BlogEntry entry);
        void DeleteBlogEntry(int entryID);
        int GetEntriesCount();
        int GetEntriesInTagCount(string tag);
        BlogEntry FindEntryByID(int entryID);
        IQueryable<BlogEntry> FindEntriesInPage(int pageNumber, int pageSize);
        IQueryable<BlogEntry> FindAllEntries();
        IQueryable<BlogEntry> FindEntriesByTag(string tag, int pageNumber, int pageSize);

        void AddComment(Comment comment);
        void DeleteComment(int commentID);
        IQueryable<Comment> FindEntryComments(int entryID);

        void CommitChanges();
    }
}
