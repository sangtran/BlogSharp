﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

namespace BlogSharp.Models
{
    public partial class Tag
    {
        public int TimesApplied { get; set; }
    }
}
