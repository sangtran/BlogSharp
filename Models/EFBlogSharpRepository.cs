﻿#region License
//
// Copyright 2010 Buu Nguyen (http://www.buunguyen.net/blog)
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;

namespace BlogSharp.Models
{
    public class EFBlogSharpRepository : IBlogSharpRepository
    {
        [Dependency]
        public BlogSharpEntities DB { get; set; }

        #region Tags
        public IQueryable<Tag> FindAllTags()
        {
            var results = from tag in DB.Tag
                          select new { Tag = tag, TimesApplied = tag.BlogEntries.Count() };
            var tags = new List<Tag>();
            results.ToList().ForEach(result =>
            {
                result.Tag.TimesApplied = result.TimesApplied;
                tags.Add(result.Tag);
            });
            return tags.AsQueryable();
        }

        public void AddTag(Tag tag)
        {
            DB.AddToTag(tag);
        }

        public IQueryable<Tag> FindTagsByNames(IEnumerable<string> tagNames)
        {
            var containExpression = DB.BuildContainsExpression<Tag, string>(t => t.Name, tagNames);
            var existingTags = DB.Tag.Where(containExpression).ToList();
            foreach (var tagName in tagNames)
            {
                if (!existingTags.Any(t => t.Name == tagName))
                {
                    var newTag = new Tag { Name = tagName };
                    AddTag(newTag);
                    existingTags.Add(newTag);
                }
            }
            return existingTags.AsQueryable();
        }
        #endregion

        #region Users
        public User ValidateUser(string loginName, string password)
        {
            return DB.User.FirstOrDefault(u => u.LoginName == loginName && u.Password == password);
        }

        public User FindUserByID(int userID)
        {
            return DB.User.FirstOrDefault(u => u.ID == userID);
        }

        public User FindUserByLoginName(string loginName)
        {
            return DB.User.FirstOrDefault(u => u.LoginName == loginName);
        }

        public User FindUserByOpenID(string openID)
        {
            return DB.User.FirstOrDefault(u => u.OpenID == openID);
        }

        public void AddUser(User user)
        {
            DB.AddToUser(user);
        }

        public User GetBlogOwner()
        {
            return DB.User.FirstOrDefault(u => u.IsOwner);
        }
        #endregion

        #region Blog Entries
        public BlogEntry FindEntryByID(int entryID)
        {
            return DB.BlogEntry.Include("Tags").Include("User")
                               .First(entry => entry.ID == entryID);
        }

        public IQueryable<BlogEntry> FindEntriesInPage(int pageNumber, int pageSize)
        {
            return
                DB.BlogEntry.Include("Tags").Include("User")
                            .OrderByDescending(entry => entry.PublishedDate)
                            .Skip(pageSize * pageNumber)
                            .Take(pageSize);
        }

        public IQueryable<BlogEntry> FindAllEntries()
        {
            return DB.BlogEntry.Include("Tags").Include("User")
                                               .OrderByDescending(entry => entry.PublishedDate);
        }

        public int GetEntriesCount()
        {
            return DB.BlogEntry.Count();
        }

        public IQueryable<BlogEntry> FindEntriesByTag(string tag, int pageNumber, int pageSize)
        {
            return
                DB.BlogEntry.Include("Tags").Include("User")
                            .Where(entry => entry.Tags.Any(t => t.Name == tag))
                            .OrderByDescending(entry => entry.PublishedDate)
                            .Skip(pageSize * pageNumber)
                            .Take(pageSize);
        }

        public int GetEntriesInTagCount(string tag)
        {
            return DB.BlogEntry.Where(entry => entry.Tags.Any(t => t.Name == tag)).Count();
        }

        public void AddBlogEntry(BlogEntry entry)
        {
            DB.AddToBlogEntry(entry);
        }

        public void UpdateBlogEntry(BlogEntry entry)
        {
            var existingEntry = DB.GetById<BlogEntry>(entry.ID);
            existingEntry.ModifiedDate = DateTime.Now;
            existingEntry.Title = entry.Title;
            existingEntry.Content = entry.Content;
            existingEntry.Tags = entry.Tags;
        }

        public void DeleteBlogEntry(int entryID)
        {
            var entry = DB.GetById<BlogEntry>(entryID);
            DB.DeleteObject(entry);
        }
        #endregion

        #region Comments
        public IQueryable<Comment> FindEntryComments(int entryID)
        {
            return DB.Comment.Include("User").Where(comment => comment.BlogEntry.ID == entryID);
        }

        public void AddComment(Comment comment)
        {
            DB.AddToComment(comment);
        }

        public void UpdateComment(Comment comment)
        {
            var existingComment = DB.GetById<Comment>(comment.ID);
            existingComment.Content = comment.Content;
        }

        public void DeleteComment(int commentID)
        {
            var comment = DB.GetById<Comment>(commentID);
            DB.DeleteObject(comment);
        }
        #endregion

        public void CommitChanges()
        {
            DB.SaveChanges();
        }
    }
}
