﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<BlogSharp.ViewModels.BlogEntriesDisplayModel>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <% foreach (var entry in Model.Entries) { %>
        <div class="entry">
            <h2><%= Html.ActionLink(entry.Title, "Show", new {entry.ID}) %></h2>
            <div>
            <%
                // Should encode here because the 300 cut-off might break the resulted HTML
                var content = Html.Encode(entry.Content);
                content = content.Length > 300
                      ? (content.Substring(0, 300) + " " + Html.ActionLink("(read more...)", "Show", new {entry.ID}))
                      : content;
            %>
            <%= content %>
            </div>
            <div class="info">Published by <%= entry.User.DisplayName %> on <%= entry.PublishedDate.ToLongDateString() %></div>
        </div>
    <% } %>
    
    <% Html.RenderPartial("Pager", Model.PagerModel); %>
</asp:Content>
