﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BlogEntryModel>" %>
<%@ Import Namespace="BlogSharp.ViewModels"%>

<% Html.EnableClientValidation(); %>
<%= Html.ValidationSummary(true, "Blog entry was not saved.  Please fix the error(s) below.") %>                
<% using (Html.BeginForm()) { %>

    <%= Html.EditorForModel() %>

    <p><input type="submit" value="Publish" /></p>
    
    <script type="text/javascript" >
        onLoad(function() {
            mySettings.previewTemplatePath = "/scripts/markitup/markitup/templates/preview.html";
            $("#Content").markItUp(mySettings);
        });
    </script>
<% } %> 