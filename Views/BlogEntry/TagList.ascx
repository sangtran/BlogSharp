﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>

<% if (!string.IsNullOrEmpty(Model)) { %>
    <div class="entry-taglist">
        <span>Tagged under: </span>
        <% var tags = Model.Split(','); %>
        <% foreach (var tag in tags) { %>
            <a class="entry-tag" href="<%= Url.Action("ShowByTag", "BlogEntry", new { tag }) %>"><b><%= tag %></b></a>
        <% } %>
    </div>
<% } %>