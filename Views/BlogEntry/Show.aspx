﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<BlogEntryModel>" %>
<%@ Import Namespace="BlogSharp.Infrastructure.Utils"%>
<%@ Import Namespace="BlogSharp.ViewModels"%>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%= Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%= Model.Title %></h2>
    <% if (Request.IsBlogOwner(Page.User)) { %>
        <div>
            <%= Html.ActionLink("Edit this post", "Edit", new {id = Model.ID}) %> | 
            <a id="delete-post" href="#">Delete this post</a>
        </div>
    <% } %>
    <br />
    <div><%= Model.Content %></div>
    <br />
    <% Html.RenderPartial("TagList", Model.Tags); %>
    <div class="info">Published by <%= Model.UserName %> on <%= Model.PublishedDate.ToLongDateString() %></div>
    
    <div>
         <fieldset>
                <legend id="comment-count"></legend>
                <div id="commentlist">
                    <input type="hidden" id="can-post-comments" value="<%= Request.IsAuthenticated %>" />
                    <ol id="thecomments"></ol>
                </div>
         </fieldset>
    </div>
    
    
    <script type="text/javascript">
        onLoad(function() {
            $("#delete-post").click(function(event) {
                event.preventDefault();
                if (confirm("Are you sure you want to delete this post and all of its comments?")) {
                    $.ajax({
                        type: "POST",
                        url: '<%= Url.Action("Delete") %>',
                        data: {entryID: <%= Model.ID %>},
                        success: function(result) {
                            window.location = '<%= Url.Action("Index") %>';
                        }
                    });
                }
            });
            comments.init(<%= Model.ID %>);
        });
    </script>
</asp:Content>
