﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Tag>>" %>
<%@ Import Namespace="BlogSharp.Infrastructure.Utils"%>
<%@ Import Namespace="BlogSharp.Models"%>

<div class="module">
    <ul class="tag-index">		
        <% int totalAppliedTimes = Model.Select(t => t.TimesApplied).Sum(); %>
        <% foreach (var tag in Model) { %>
            <li class="<%= Html.TagClass(tag, totalAppliedTimes) %>">
                <%= Html.ActionLink(tag.Name, "ShowByTag", "BlogEntry", new {tag = tag.Name}, null) %>
            </li>       
        <% } %>
    </ul>
</div>