﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BlogSharp.ViewModels.PagerModel>" %>
<%@ Import Namespace="BlogSharp.ViewModels"%>

<%
    if (Model.TotalEntries <= Model.EntriesPerPage) 
        return;
    
    var totalPages = (Model.TotalEntries / Model.EntriesPerPage) + 
        ((Model.TotalEntries % Model.EntriesPerPage == 0) ? 0 : 1);
    var urlTemplate = Model.PageUrlTemplate;
    var sb = new StringBuilder();
    for (int pageIndex = 0; pageIndex < totalPages; pageIndex++)
    {
        var @class = Model.CurrentPageIndex == pageIndex ? " class=\"active\"" : string.Empty;
        var link = urlTemplate.Replace(PagerModel.PageSymbol, pageIndex.ToString());
        sb.Append(string.Format("<li{0}><a href=\"{1}\" /><span>{2}</span></a>", @class, link, pageIndex + 1));
    }
    var html = sb.ToString();
    if (html != string.Empty)
    {
        Response.Write("<ul class=\"per-page\">" + sb + "</ul>");
    }
%>