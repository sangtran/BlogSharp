﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="BlogSharp.Infrastructure.Config"%>
<%@ Import Namespace="BlogSharp.Infrastructure.Utils"%>
<%
    if (Request.IsAuthenticated) {
%>
        Welcome <b><%= Html.Encode(((EnhancedPrincipal)Page.User).Data.DisplayName) %></b>!
        [ <%= Html.ActionLink("Log Off", "LogOff", "Account", new { returnurl = Request.Path.UrlEncode() }, null) %> ]
<%
    }
    else {
%> 
        [ <%= Html.ActionLink("Log On", "LogOn", "Account", new { returnurl = Request.Path.UrlEncode() }, null) %> ]
<%
    }
%>
