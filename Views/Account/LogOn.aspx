﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LogOnModel>" %>
<%@ Import Namespace="BlogSharp.ViewModels"%>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        onLoad(function() {
            openid.img_path = '/Scripts/openid-selector/images/';
            openid.init('openid_identifier');
            $("#openid_identifier").focus();
        });
    </script>
    <h2>Log On</h2>
    <p>
        Please enter your username and password. <%= Html.ActionLink("Register", "Register") %> if you don't have an account.
        You can also use OpenID to login.
    </p>
    
    <% Html.EnableClientValidation(); %>
    <%= Html.ValidationSummary(true, "Login was unsuccessful. Please correct the errors and try again.") %>        
            
    <% using (Html.BeginForm("LogOn", "Account", null, FormMethod.Post, new { id = "login_form" }))
       { %>
        <div>   
            <fieldset>
                <legend>Account Information</legend>
                <%= Html.EditorForModel() %>
                
                <%-- Could have this rendered by EditorForModel(), but ASP.NET MCV RC 2 contains bug which makes RememberMe required --%> 
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.RememberMe) %>
                    <%= Html.CheckBoxFor(m => m.RememberMe)%>
                </div>
                            
                <p>
                    <input type="submit" value="Log On" />
                </p>
            </fieldset>
        </div>
    <% } %> 
    
    <% using (Html.BeginForm("AuthenticateOpenID", "Account", null, FormMethod.Post,  new {id="openid_form"})) { %>   
        <div>
            <fieldset> 
                <legend>OpenID</legend>     
                       
     	        <div id="openid_choice">
	    		    <p>Please click your account provider:</p>
	    		    <div id="openid_btns"></div>
			    </div>
    			
			    <div id="openid_input_area" class="editor-field">
				    <%= Html.TextBox("openid_identifier", null, new { @class="text", value="http://" }) %> 
			    </div>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.RememberMe) %>
                    <%= Html.CheckBoxFor(m => m.RememberMe)%>
                    <%= Html.HiddenFor(m => m.ReturnUrl) %>
                </div>
            </fieldset>
        </div>
    <% } %>  
</asp:Content>
