﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SetupModel>" %>
<%@ Import Namespace="BlogSharp.ViewModels"%>

<asp:Content ID="setupTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Setup Blog#
</asp:Content>

<asp:Content ID="setupContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Setup Blog#</h2>
    <% Html.EnableClientValidation(); %>
    <%= Html.ValidationSummary(true, "Account creation was unsuccessful. Please correct the errors and try again.") %>
    <% using (Html.BeginForm()) { %>
        <div>
            <fieldset>
                <legend>Setup Blog#</legend>
                <%= Html.EditorForModel() %>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.About) %>
                </div>
                <div class="editor-field">
                    <%= Html.TextAreaFor(m => m.About) %>
                    <%= Html.ValidationMessageFor(m => m.About) %>
                </div>
                
                <p>
                    <input type="submit" value="Finish" />
                </p>
            </fieldset>
        </div>
    <% } %>
</asp:Content>
