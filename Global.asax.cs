﻿using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BlogSharp.Controllers;
using BlogSharp.Infrastructure.Config;
using BlogSharp.Infrastructure.IoC;
using BlogSharp.Models;
using Combres;
using log4net.Config;
using Microsoft.Practices.Unity;

namespace BlogSharp
{
    public class MvcApplication : HttpApplication, IContainerProvider
    {
        static MvcApplication()
        {
            XmlConfigurator.Configure();
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.AddCombresRoute("Combres");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Delete Comment",
                "entries/{entryID}/comments/{commentID}/delete",
                new { controller = "BlogEntry", action = "DeleteComment" }
                );

            routes.MapRoute(
                "Get Comments",
                "entries/{entryID}/comments",
                new { controller = "BlogEntry", action = "GetComments" }
                );

            routes.MapRoute(
                "Add Comment",
                "entries/{entryID}/comment",
                new { controller = "BlogEntry", action = "AddComment" }
                );

            routes.MapRoute(
                "Show By Tag",
                "entries/tag/{tag}",
                new { controller = "BlogEntry", action = "ShowByTag" }
                );

            routes.MapRoute(
                "Blog Entry",
                "entries/{action}/{id}",
                new { controller = "BlogEntry", id = "" }
            );

            routes.MapRoute(
                "Default",                                                  // Route name
                "{controller}/{action}/{id}",                               // URL with parameters
                new { controller = "BlogEntry", action = "Index", id = "" } // Parameter defaults
            );
        }        
        
        private static IUnityContainer UnityContainer;

        public IUnityContainer Container
        {
            get { return UnityContainer; }
        }

        protected void Application_Start()
        {
            InitializeIoC();
            RegisterRoutes(RouteTable.Routes);
        }

        private static void InitializeIoC()
        {
            UnityContainer = new UnityContainer();
            UnityContainer
                // BlogSharpEntities has multiple contructors, need to tell Unity which one to invoke
                .RegisterType<BlogSharpEntities>(new PerRequestLifetimeManager(), new InjectionConstructor())
                .RegisterType<IBlogSharpRepository, EFBlogSharpRepository>(new TransientLifetimeManager())
                .RegisterType<IController, BlogEntryController>("BlogEntry", new TransientLifetimeManager())
                .RegisterType<IController, AccountController>("Account", new TransientLifetimeManager());
            ControllerBuilder.Current.SetControllerFactory(typeof(UnityControllerFactory));
        }

        protected void Application_AuthenticateRequest()
        {
            if (IgnoreAuthenticateRequest())
                return;
            EnhancedPrincipal user;
            if (User != null &&
                User.Identity.IsAuthenticated &&
                User.Identity.AuthenticationType == "Forms")
            {
                var repository = (IBlogSharpRepository)Container.Resolve(typeof(IBlogSharpRepository));
                var dbUser = repository.FindUserByID(int.Parse(User.Identity.Name));
                user = new EnhancedPrincipal(User.Identity, dbUser);
            }
            else
            {
                user = EnhancedPrincipal.Anonymous;
            }
            HttpContext.Current.User = Thread.CurrentPrincipal = user;
        }

        private static readonly string[] IgnoredExtensions = new[]
                                                                 {
                                                                     ".js", ".css", ".txt", ".html", ".htm",
                                                                     ".xml", ".png", ".gif", ".jpg", ".ico"
                                                                 };
        
        private static bool IgnoreAuthenticateRequest()
        {
            var url = HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant();
            return IgnoredExtensions.Any(url.EndsWith);
        }
    }
}